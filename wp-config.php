<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, WordPress Language, and ABSPATH. You can find more information
 * by visiting {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */
 
// Include local configuration
if (file_exists(dirname(__FILE__) . '/local-config.php')) {
	include(dirname(__FILE__) . '/local-config.php');
}

// Global DB config
if (!defined('DB_NAME')) {
	define('DB_NAME', 'db142268_yfn');
}
if (!defined('DB_USER')) {
	define('DB_USER', 'db142268');
}
if (!defined('DB_PASSWORD')) {
	define('DB_PASSWORD', 'fingered!');
}
if (!defined('DB_HOST')) {
	define('DB_HOST', 'internal-db.s142268.gridserver.com');
}

/** Database Charset to use in creating database tables. */
if (!defined('DB_CHARSET')) {
	define('DB_CHARSET', 'utf8');
}

/** The Database Collate type. Don't change this if in doubt. */
if (!defined('DB_COLLATE')) {
	define('DB_COLLATE', '');
}

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '3_9ypP@B`_togRYjznO4<{IixMQS,k4-9wz>s9L*`Q`l./T7p)df.6Q+[}-OKw0/');
define('SECURE_AUTH_KEY',  '7v;N{M@vihm#%+r*fR7yWLk#/^8LM4*0|Eqi@7@.&m2>n^!< ${sgGw)QuzU@pM&');
define('LOGGED_IN_KEY',    'i(y`HWy~6qJS/wSb?_ +1|zPC.;~+$EZ~>3,ce1T~yZ+(01jCf#`{pp<----LxM;');
define('NONCE_KEY',        'iRMv+;d ?bOzTZ~eng:- I^<2cutzC-U.B[!YSEW6>#[a<gb+6-2(e`dtJhKj(v.');
define('AUTH_SALT',        ']m>YS]k.!%0g/X>V/}PI1XRmk1AFTh*sz<i^4f#`R^ohnn-!|0+#<RLDf^Y7JD]N');
define('SECURE_AUTH_SALT', 'ea$PBn4Vj9)d+VXXo $G<hNfZN1Z<e; IXA+oUh=m0oL&VwJwRC6o2j*lhgO!-ht');
define('LOGGED_IN_SALT',   'dV$7PDId{$*RK6%wD~S9*bUu%%|L*xiWU7Rc^yHU[jncU2-HDTB9HlHk{Q~XIz1i');
define('NONCE_SALT',       'a0=jc|Lv!Vh}~.vZ $+g$u_-jC>Sd+gMr3[qM~-pt|Y&eQo!q*PHH2^[v<DzW[$R');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'sbm_';

/**
 * WordPress Localized Language, defaults to English.
 *
 * Change this to localize WordPress. A corresponding MO file for the chosen
 * language must be installed to wp-content/languages. For example, install
 * de_DE.mo to wp-content/languages and set WPLANG to 'de_DE' to enable German
 * language support.
 */
define('WPLANG', '');



/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
if (!defined('WP_DEBUG')) {
	define('WP_DEBUG', false);
}

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');

<?php get_header(); ?>

<section id="page-header">

	<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/internal-page-header.jpg" class="img-responsive img-center"/>

</section>

<section id="page">

  <div class="container">

  <h1>

  	<?php single_cat_title( '', true ); ?>

          </h1>

    <div class="row">

      <div class="col-size-7">

         

		  <?php		

		   while ( have_posts() ) : the_post(); ?>

           <div class="live-it-now">

              

				<iframe width="100%" height="330px" src="https://www.youtube.com/embed/<?php echo get_post_meta(get_the_ID(),'you_tube_url',true); ?>?rel=0&amp;showinfo=0" frameborder="0" allowfullscreen></iframe>

                <div class="live-it-now-content">

                	<span>Published on <?php echo get_the_date();?></span>

			<a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>"><h2><?php the_title(); ?></h2></a>

                	<?php the_content(); ?>

                </div>

          	</div>

          <?php endwhile; ?>

          <?php if (  $wp_query->max_num_pages > 1 ) : 

		  		//rewrite_rule has been applied for it works correctly see inc/routes.php

		  ?>

				<div id="nav-below" class="navigation">

					<div class="nav-previous"><?php next_posts_link('<span class="meta-nav">&larr;</span> Older posts'); ?></div>

					<div class="nav-next"><?php previous_posts_link( 'Newer posts <span class="meta-nav">&rarr;</span>' ); ?></div>

				</div><!-- #nav-below -->

<?php endif; ?>

      </div>

      <aside>

      <div class="col-size-5">

        <?php get_sidebar( 'tiles' ); ?>

      </div>

      </aside>

    </div>

  </div>

</section>

<?php get_footer(); ?>


<section id="copyright">
  	<div class="container">
    	<div class="row">
        	<div class="col-size-7">
            	<div class="copy">
                © <?php echo date('Y');?> Your Future Now | <a href="#">Terms & Conditions</a> | <a href="#">Privacy Policy</a></div>
          </div>
            <div class="col-size-5">
            <div class="sbm">
          Website Design by <a href="#">Spicy Broccoli Media </a></div></div>
    	</div>
    </div>
  </section>
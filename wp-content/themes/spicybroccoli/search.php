<?php get_header(); ?>
<section id="page-header">
	<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/internal-page-header.jpg" class="img-responsive img-center"/>
</section>
<section id="page" class="search">
  <div class="container">
    <div class="row">
      <div class="col-size-7">
          <article>
          <h1>
            Search results for: <?php echo get_search_query(); ?>
          </h1>
          <?php if(have_posts()): ?>
				<ul id="search-list">
				<?php while(have_posts()): the_post(); ?>
                <li>
                    <h2> <a href="<?php the_permalink(); ?> "><?php the_title(); ?></a></h2>
          			<p><?php echo getLimitedText(get_the_content()); ?>
                    <a href="<?php the_permalink(); ?> ">Read More</a>  </p>
                    
                    </li>
				<?php endwhile; ?>
                <ul>
                <?php else: ?>
                    <p>Sorry we couldn't find anything that matched <strong><?php echo get_search_query(); ?></strong></p>
                <?php endif; ?>
                
		</article>
      </div>
      <aside>
      <div class="col-size-5">
        <?php get_sidebar( 'tiles' ); ?>
      </div>
      </aside>
    </div>
  </div>
</section>
<?php get_footer(); ?>

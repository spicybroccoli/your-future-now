
            <?php while( $the_query->have_posts() ) : $the_query->the_post() ?>
            <li >
            <blockquote>
              <div class="col-size-2 hidden-md hidden-sm"><img src="<?php 
			$img = wp_get_attachment_image_src( get_post_thumbnail_id( get_the_ID() ), 'single-post-thumbnail' );
			if(!empty($img['0'])){
				echo $img['0']; 
			}else{
				echo get_stylesheet_directory_uri() . "/images/imgs/no-user.gif";
			}
	?>" class="img-responsive img-center"/></div>
              <div class="col-size-10 ">
              <div class="testimonial">
                  <div class="col-sm-12 col-md-2">
      	            <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/imgs/img_testimonial.png" class="img-responsive img-center"/>
                  </div>
                  <div class="col-sm-12 col-md-10">
					<?php the_content(); ?>
                    <strong><?php the_title(); ?></strong>
                  </div>
                </div>
              </div>
              </blockquote>
            </li>
            <?php endwhile; ?>
<?php

/**

 * Theme Functions

 */



/**

 * Set the content width based on the theme's design and stylesheet.

 * Used to set the width of images and content. Should be equal to the width the theme

 * is designed for, generally via the style.css stylesheet.

 */

if ( ! isset( $content_width ) )

	$content_width = 640;



/** Tell WordPress to run twentyten_setup() when the 'after_setup_theme' hook is run. */

add_action( 'after_setup_theme', function() {



	// This theme uses post thumbnails

	add_theme_support( 'post-thumbnails' );

	add_theme_support( 'html5', array( 'comment-list', 'comment-form', 'search-form', 'gallery', 'caption' ) );



	// This theme uses wp_nav_menu() in one location.

	register_nav_menus( array(

			'primary' => 'Primary Navigation',

			'instant-access' => 'Lead Magnet Navigation'

		) );



});



// Bootstrap Theme

include('inc/template-utils.php'); // Load Template Utilities

include('inc/branding.php'); // Load Template Utilities

include('inc/responsive-nav.php'); // Responsive Nav

// include('inc/landing.php'); // Load Landing page if necessary

include('inc/scripts.php'); // Load Scripts

include('inc/routes.php');

// CPT's

include('inc/cpt-course.php');

include('inc/cpt-event.php');

include('inc/cpt-testimonal.php');

include('inc/cpt-lead-magnet.php');





function getTileClass($vars) {

	$box_title = get_post_meta(get_the_ID(),'box_title',true);

	$box_type = get_post_meta(get_the_ID(),'box_type',true);

	$class_add = get_post_meta(get_the_ID(),'additional_class',true);

	$post_type = get_post_type( get_the_ID() );

	

	$class_add = ($class_add)? ' ' . $class_add : '';

	

	switch($box_type){

	case 'Large Image' : $class_ini = 'tile-large';

			break;	

	case 'Left Image' : $class_ini = 'tile-small img-left';

			break;

	case 'Right Image' : $class_ini = 'tile-small img-right';

			break;

	case 'No Image' : $class_ini = 'tile-small no-image';

			break;	

	}

	

	switch($post_type){

		case 'testimonial' : $class_ini = 'tile-small img-right';

								$class_add = '';

								$box_title = 'testimonial';

				break;	

		default : $box_title = strtolower((preg_replace('/ /', '-', $box_title)));

				break;

	}

	

	return $class_ini . $class_add . " " . $box_title;

	

}

function getTileLink($var, $link) {

	

	switch($var){

		case 'No Image' : $txt = 'Register Now';

			break;

		default :  $txt = 'More Info';

					$class = 'tile-more-i ';

			break;

	}

	

	(get_post_type( get_the_ID() ) == 'testimonial')?$txt = 'Read More':'';

	

	return '<a href="'.$link.'" class="'.$class.'">'.$txt.'</a>';

	

}



function getTileImg($var, $w = true, $h = true) {

	

	$post_type = get_post_type( get_the_ID() );

	

	switch($post_type){

		case 'testimonial' :	$image = wp_get_attachment_image_src( get_post_thumbnail_id( get_the_ID() ), array($w, $h) );

								$image = $image[0];

			break;

		default :  	$image = (get_field('image_url'));

					$image = $image['url'];

			break;

	}

	

	return $image;

	

}



// FIX DE BUG INTO THE MENU ON CUSTOM PAGES

function getMainMenu($menulocation, $args = array()){

  $locations = get_nav_menu_locations();

  $args[] = array('theme_location' => $menulocation);

  $menuItems = wp_get_nav_menu_items( $locations[ $menulocation ] );

    if(empty($menuItems))

      return false;

    else{

      wp_nav_menu($args);

      return true;

    }

}



/* limit the content without split a word

* DO NOT USE the_content()

* USE get_the_content() INSTEAD

*/

function getLimitedText($content, $size = 200, $dodots = true, $strip = true) {

		

		($strip)?$content = wp_strip_all_tags($content):'';

		

		if(strlen($content) > $size){

			$pos = stripos( $content, ' ', $size);

			$content = substr( $content, 0, $pos ); 

			

			$dots = ($dodots)?'...':'';

		}

		

	return $content . $dots;

}



add_filter( 'the_content_more_link', 'modify_read_more_link' );

function modify_read_more_link() {

return '<a class="more-link" href="' . get_permalink() . '">Continue reading...</a>';

}



add_action('init', 'add_my_rule');



    function add_my_rule()

    {

        global $wp;

        $wp->add_query_var('args');



        add_rewrite_rule('instant_access\/(.*)\/show','index.php?page_type=instant_access&pagename=$matches[1]&args=show','top');

    }
	
	 
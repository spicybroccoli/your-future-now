<?php  
	//GET IN TOUCH TEMPLATE
	get_template_part('content', 'get-in-touch');
	
	//GET FOOTER LINKS TEMPLATE
	get_template_part('content', 'footer-links');
	
	//GET FOOTER COPYRIGHT TEMPLATE
	get_template_part('content', 'footer-copyright');
	
?>   
</div><!-- #wrapper -->
</div>

<?php wp_footer(); ?>
<script src="<?php echo get_stylesheet_directory_uri(); ?>/js/app.js"></script>
<?php if(is_home()) : ?>
<script src="<?php echo get_stylesheet_directory_uri(); ?>/js/app-tiles.js"></script>
<?php endif; ?>
<script>	new mlPushMenu( document.getElementById( 'mp-menu' ), document.getElementById( 'trigger' ) );</script>
</body>
</html>

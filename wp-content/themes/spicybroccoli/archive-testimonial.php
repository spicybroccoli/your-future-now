<?php get_header(); ?>
<section id="page-header">
	<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/internal-page-header.jpg" class="img-responsive img-center"/>
</section>
<section id="our-testimonials">
  <div class="container">
    <div class="row">
      <div class="col-size-12">
        <h1>OUR TESTIMONIALS</h1>
        <p>Find out what our clients have to say about us</p>
        <div class="our-testimonials-content">
          <ul>
			<?php 
                //WP QUERY ARGS
                $args = array(
                    'post_type' => array( 'testimonial' ),
                    'order' => 'ASC',
                    'orderby' => 'rand',
                );
                
                $the_query = new WP_Query( $args );
                
                include(locate_template('content-testimonials.php'));
            ?>
			</ul>
        </div>
      </div>
    </div>
  </div>
</section>
<?php  
	//GET FOOTER COPYRIGHT TEMPLATE
	get_footer();
	
?> 

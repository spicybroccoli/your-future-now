<?php get_header(); ?>
<section id="tiles">
  	<div class="container-tiles" id="container-tiles-home">
    <div class="container-tiles-row">
    
    <?php 
	//WP QUERY ARGS
	$args = array(
		'post_type' => array( 'course', 'testimonial', 'instant_access' ),
		'meta_query' => array(
			'relation' => 'AND',
			array(
				'key' => 'feature_on',
				'value' => 'Home Page',
				'compare' => 'LIKE'
			)
		),
		'order' => 'ASC',
		'orderby' => 'meta_value_num',
		'meta_key' => 'home_page_order'
	);
	
	$the_query = new WP_Query( $args );
			
	if( $the_query->have_posts() ) : ?>
	<ul>
    <?php while( $the_query->have_posts() ) : $the_query->the_post() ?>
     <li class="tile <?php echo getTileClass($the_query); ?>">
    <div class="tile-wrap">
    	<div class="tile-img">
        	<img src="<?php echo getTileImg( $the_query ); ?>" alt="<?php echo get_post_meta(get_the_ID(),'box_title',true); ?>"/>
      	</div>
        <div class="tile-content">
       		<h3><?php echo get_post_meta(get_the_ID(),'box_title',true); ?></h3>
            <p><?php echo get_post_meta(get_the_ID(),'box_text',true); ?></p>
            <div class="tile-content-bottom">
            <?php
			if(get_field('logo_url')) : ?>
            <?php $logo = (get_field('logo_url')); ?>
                <img src="<?php echo $logo['url'] ?>" class="tile-logo" width="70"  alt=""/> 
            <?php endif; ?>
            <?php echo getTileLink(get_post_meta(get_the_ID(),'box_type',true), get_permalink(get_the_ID())); ?>
            </div>
        </div>
     </div>
		</li>
	<?php endwhile; ?>
     </ul>
     <?php else: ?>
     Not yet
<?php endif; ?>

    
   <?php wp_reset_query();	 // Restore global post data stomped by the_post(). ?>
   
  </section>
  <?php get_footer(); ?>
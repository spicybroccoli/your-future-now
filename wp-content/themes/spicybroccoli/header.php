<!DOCTYPE html>

<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>" />
<title>
<?php

	/*

	 * Print the <title> tag based on what is being viewed.

	 */

	global $page, $paged;



	wp_title( '|', true, 'right' );



	// Add the blog name.

	bloginfo( 'name' );



	// Add the blog description for the home/front page.

	$site_description = get_bloginfo( 'description', 'display' );

	if ( $site_description && ( is_home() || is_front_page() ) )

		echo " | $site_description";



	// Add a page number if necessary:

	if ( $paged >= 2 || $page >= 2 )

		echo ' | ' . sprintf( __( 'Page %s', 'twentyten' ), max( $paged, $page ) );



	?>
</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo( 'stylesheet_url' ); ?>" />
<link rel="stylesheet" type="text/css" media="all" href="<?php echo get_stylesheet_directory_uri(); ?>/style-responsive.css" />
<script src="<?php echo get_stylesheet_directory_uri(); ?>/js/modernizr.js"></script>
<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<?php if( get_query_var('show_header') != 'noshow' ) : ?>
<div id="mp-pusher" class="mp-pusher">
<nav class="mp-menu" id="mp-menu">
  <div class="mp-level">
    <?php

	           	wp_nav_menu( 

	            	array( 

	            		'menu' => 'primary',

	            		'container'  => 'false',

	            		'container_id' => 'mp-menu',

	            		'menu_class' => 'mp-level',

	            		'container_class' => 'mp-menu',

	            		'menu_class' => '',  

	            		'walker' => new SBM_Responsive_Nav()

	            	) 

	            ); 

	        ?>
  </div>
</nav>
<div id="wrapper" class="hfeed">
<section id="header">
  <div class="container">
  <div class="row"> <a href="#" id="trigger" class="menu-trigger hidden-large"></a>
    <div class="logo col-size-2 col-md-5"> <a href="<?php echo get_site_url(); ?>"  alt="Your Future Now"> <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/YFN_logo.png"  title="Your Future Now"/> </a></div>
    
    <!-- 

<div style="display:none" class="search col-size-2 col-md-4 hidden-sm" class="search">

        <form role="search" method="get" class="search-form" action="http://dev.spicybroccolitesting.com.au/your-future-now/">

          <label>

            <input type="search" class="search-field" placeholder="Search" value="" name="s" title="Search">

          </label>

        </form>

      </div>

-->
    
    <div class="col-size-10">
      <div class="header-menu condensed">
        <?php

	           	if(!getMainMenu('primary')){

					  $backup = $wp_query;

					  $wp_query = NULL;

					  $wp_query = new WP_Query(array('post_type' => 'post'));

					  getMainMenu('primary');

					  $wp_query = $backup;

					}

	        ?>
      </div>
    </div>
  </div>
</section>
<!-- header -->

<?php endif; ?>
<?php if(is_front_page()){ ?>
<section id="slider">
  <?php 

			echo do_shortcode("[metaslider id=30]"); 

		?>
</section>
<!-- slider -->

<section id="transform-yourself">
  <div class="container">
    <h2>transform yourself in...</h2>
    <div class="buttons">
      <div class="col-size-4 col-off-pad">
        <div class="btn-wide life arrow-top"><a href="<?php echo site_url(); ?>/need-help/emotions/">Life</a></div>
      </div>
      <div class="col-size-4 col-off-pad">
        <div class="btn-wide wealth arrow-top"><a href="<?php echo site_url(); ?>/need-help/finance/">Wealth</a></div>
      </div>
      <div class="col-size-4 col-off-pad">
        <div class="btn-wide relationships arrow-top"><a href="<?php echo site_url(); ?>/need-help/relationships/">Relationships</a></div>
      </div>
    </div>
  </div>
</section>
<!-- transform yourself -->

<?php } ?>

<section id="footer-links">
      <div class="container">
      	<div class="row">
       	  <div class="col-five">
            <h4>About Us</h4>
            <?php
	           	wp_nav_menu( 
	            	array( 
	            		'menu' => 'primary',
						'depth' => '1',
	            		'menu_class' => '',
	            		'container' => ''
	            	) 
	            ); 
	        ?>
        	</div>
          <div class="col-five">
            <h4>Courses</h4>
            <?php
	           	wp_nav_menu( 
	            	array( 
	            		'menu' => 'primary',
						'depth' => '1',
	            		'menu_class' => '',
	            		'container' => ''
	            	) 
	            ); 
	        ?>
            </div>
            <div class="col-five">
            <h4>Events</h4>
            	<?php
	           	wp_nav_menu( 
	            	array( 
	            		'menu' => 'primary',
						'depth' => '1',
	            		'menu_class' => '',
	            		'container' => ''
	            	) 
	            ); 
	        ?>
            </div>
          <div class="col-five">
            <h4>Need Help</h4>
            <?php
	           	wp_nav_menu( 
	            	array( 
	            		'menu' => 'primary',
						'depth' => '1',
	            		'menu_class' => '',
	            		'container' => ''
	            	) 
	            ); 
	        ?>
            </div>
          <div class="col-five">
            <h4>Need Help</h4>
            <?php
	           	wp_nav_menu( 
	            	array( 
	            		'menu' => 'primary',
						'depth' => '1',
	            		'container' => ''
	            	) 
	            ); 
	        ?>
            </div>
        </div>
      </div>
  </section>
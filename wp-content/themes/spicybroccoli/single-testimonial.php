<?php get_header(); ?>
<section id="page-header">
	<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/internal-page-header.jpg" class="img-responsive img-center"/>
</section>
<section id="page-testimonials">
<div class="container">
<div class="our-testimonials-content">
          <ul>
<?php if ( have_posts() ) : the_post(); ?>
	<?php 
	//WP QUERY ARGS
                $args = array(
                    'post_type' => array( 'testimonial' ),
                    'p' => get_the_ID()
                );
                
                $the_query = new WP_Query( $args );
	
	include(locate_template('content-testimonials.php')); ?>
<?php endif; ?>
</ul>
</div>
</div>
</section>
<aside id="our-testimonials">
  <div class="container">
    <div class="row">
      <div class="col-size-12">
        <h1>OUR TESTIMONIALS</h1>
        <p>Find out what our clients have to say about us</p>
        <div class="our-testimonials-content">
          <ul>
			<?php 
                //WP QUERY ARGS
                $args = array(
                    'post_type' => array( 'testimonial' ),
                    'order' => 'ASC',
                    'orderby' => 'menu_order',
					'post__not_in' => array(get_the_ID())
                );
                
                $the_query = new WP_Query( $args );
                
                include(locate_template('content-testimonials.php'));
            ?>
			</ul>
        </div>
      </div>
    </div>
  </div>
</aside>
<?php  
	wp_reset_query();
	//GET FOOTER COPYRIGHT TEMPLATE
	get_footer();
	
?> 

 <?php 
	//WP QUERY ARGS
	$args = array(
		'post_type' => array( 'course', 'testimonial', 'instant_access' ),
		'meta_query' => array(
			'relation' => 'AND',
			array(
				'key' => 'feature_on',
				'value' => 'Sidebar',
				'compare' => 'LIKE'
			)
		),
		'order' => 'ASC',
		'orderby' => 'meta_value_num',
		'meta_key' => 'sidebar_order'
	);
	
	$the_query = new WP_Query( $args );
			
	if( $the_query->have_posts() ) : ?>
    
    <ul id="sidebar-tiles">
    <?php while( $the_query->have_posts() ) : $the_query->the_post() ?>
     <li class="tile <?php echo getTileClass($the_query); ?>">
    <div class="tile-wrap">
    	<?php if(get_post_type( get_the_ID() ) != 'testimonial'){ ?>
        <div class="tile-img">
        	<img src="<?php echo getTileImg( $the_query ); ?>" alt="<?php echo get_post_meta(get_the_ID(),'box_title',true); ?>"/>
      	</div>
        <?php } ?>
        <div class="tile-content">
       		<h3><?php echo get_post_meta(get_the_ID(),'box_title',true); ?></h3>
            <p><?php echo get_post_meta(get_the_ID(),'box_text',true); ?>
            
            <?php if(get_post_type( get_the_ID() ) == 'testimonial'){ ?>
            		<span class="condensed"><?php echo the_title() ?></span>
                    <img src="<?php echo getTileImg( $the_query, 85, 85 ); ?>" alt="<?php echo get_post_meta(get_the_ID(),'box_title',true); ?>"/>
                    </p>
                <?php }else{ ?>
                </p>
                <div class="tile-content-bottom">
					<?php
                    if(get_field('logo_url')) : ?>
                    <?php $logo = (get_field('logo_url')); ?>
                        <img src="<?php echo $logo['url'] ?>" class="tile-logo" width="70"  alt=""/> 
                    <?php endif; ?>
                    <?php echo getTileLink(get_post_meta(get_the_ID(),'box_type',true), get_permalink(get_the_ID())); ?>
                </div>
                
                <?php } ?>
        </div>
     </div>
		</li>
	<?php endwhile; ?>
     </ul>
    
    <?php endif; ?>

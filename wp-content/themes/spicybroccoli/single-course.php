<?php get_header(); ?>
<?php if ( have_posts() ) : the_post(); ?>
<article id="course" class="<?php echo strtolower((preg_replace('/ /', '-', $post->post_title))); ?>">
      <section id="course-header">
        <div class="container">
          <div class="row">
            <div class="col-size-12">
             <header>
                <h1><?php echo $post->post_title; ?></h1>
                <?php the_content(); ?>
              
              <?php if (get_post_meta(get_the_ID(),'course_video_url',true)) : ?>
                  <section id="course-video">
                    <iframe width="100%" height="540px" src="https://www.youtube.com/embed/<?php echo get_post_meta(get_the_ID(),'course_video_url',true); ?>?rel=0&amp;showinfo=0" frameborder="0" allowfullscreen></iframe>
                  </section>
              <?php endif; ?>
              <a href="<?php echo site_url( '/upcoming-events/' ); ?>" class="btn-book-now condensed">Book Now</a>
              </header>
            </div>
          </div>
        </div>
      </section>
  <?php if (get_post_meta(get_the_ID(),'course_reasons',true)) : ?>
  <section id="course-reasons">
    <div class="container">
      <div class="row">
        <div class="col-size-12">
        
		<?php echo get_post_meta(get_the_ID(),'course_reasons',true); ?>
			<a href="<?php echo site_url( '/upcoming-events/' ); ?>" class="btn-book-now condensed">Book Now</a>
        </div>
        
      </div>
    </div>
  </section>
  <?php endif; ?>
  <section id="course-content">
    <div class="container">
      <div class="row">
        <div class="col-size-12">
          <div class="course-content">
            <?php 
		  // GET PAGE BUILDER
		  echo do_shortcode(get_post_meta(get_the_ID(),'page_builder_code',true)); ?>
          </div>
        </div>
      </div>
    </div>
  </section>
</article>
<?php if (get_post_meta(get_the_ID(),'bottom_course_reasons',true)) : ?>
  <section id="course-reasons">
    <div class="container">
      <div class="row">
        <div class="col-size-12">
        
		<?php echo get_post_meta(get_the_ID(),'bottom_course_reasons',true); ?>

        </div>
      </div>
    </div>
  </section>
  <?php endif; ?>
<section id="our-testimonials">
  <div class="container">
    <div class="row">
      <div class="col-size-12">
        <h5>OUR TESTIMONIALS</h5>
        <p>Find out what our clients have to say about us</p>
        <div class="our-testimonials-content">
          <ul>
            <?php  
                //WP QUERY ARGS
                $args = array(
                    'post_type' => array( 'testimonial' ),
                    'posts_per_page' => 1,
                    'order' => 'ASC',
                    'orderby' => 'rand',
                );
                
                $the_query = new WP_Query( $args );
                //GET our testimonial TEMPLATE
                include(locate_template('content-testimonials.php'));
            ?>
          </ul>
        </div>
      </div>
    </div>
  </div>
</section>
<?php  
	endif;
	//GET FOOTER COPYRIGHT TEMPLATE
	get_footer();
	
?>

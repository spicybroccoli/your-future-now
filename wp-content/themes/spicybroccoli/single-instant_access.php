<?php get_header(); ?>
<section id="lead-magnet" style="background: url(<?php $image = (get_field('lead_background')); echo $image['url']; ?>) center center; background-size: cover;">
<article>
<div class="container <?php echo strtolower((preg_replace('/ /', '-', $post->post_title))); ?>">
	<div class="row">
    <div class="col-size-12">
        <h1><?php echo $post->post_title; ?></h1>
    </div>
    <div class="col-size-4">
    <img src="<?php 
	$img = wp_get_attachment_image_src( get_post_thumbnail_id( get_the_ID() ), 'single-post-thumbnail' );
	echo $img['0']; 
	?>" class="img-responsive img-center"/>
    </div>
    <div class="col-size-4">
   	  <div class="lead-content center">
      <h2>CONTENT INFORMATION</h2>
		<?php echo wpautop($post->post_content); ?>
       </div>
       
    </div>
    <div class="col-size-4">
        <div class="lead-content form">
        <h3>sales intro<br>text here</h3>
            <span><input type="text" name="contact-name" placeholder="Name" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required" aria-required="true" aria-invalid="false"></span> 
            <span><input type="text" name="contact-phone" placeholder="Phone" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required" aria-required="true" aria-invalid="false"></span> 
            <span><input type="email" name="contact-email" placeholder="Email" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-required wpcf7-validates-as-email" aria-required="true" aria-invalid="false"></span>
        </div>
        <div class="lead-content-button">
        	<a href="#">instant access</a>
        </div>
    </div>
    </div>
</article>
</section>
<?php  
	//GET FOOTER COPYRIGHT TEMPLATE
	get_template_part('footer', 'instant_access');
	
?> 
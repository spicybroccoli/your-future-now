<?php get_header(); ?>
<section id="page-header">
	<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/internal-page-header.jpg" class="img-responsive img-center"/>
</section>
<section id="page">
  <div class="container">
        <article>
          <?php if ( have_posts() ) : the_post(); ?>
          <h1>
            <?php the_title(); ?>
          </h1>
          <?php the_content(); ?>
          <?php endif; ?>
        </article>
  </div>
</section>
<?php get_footer(); ?>

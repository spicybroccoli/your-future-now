<section id="lead-magnet-menu">
<div class="container">
<div class="header-menu condensed">
        <?php
					$args = array( 
						'theme_location' => 'instant-access', 
						'menu' => 'menu-lead-magnet'
						);
					wp_nav_menu( $args );
        ?>
        </div>
        </div>
</section>
<?php  
	//GET FOOTER COPYRIGHT TEMPLATE
	get_template_part('content', 'footer-copyright');
	
?>   
</div><!-- #wrapper -->
</div>

<?php wp_footer(); ?>
<script src="<?php echo get_stylesheet_directory_uri(); ?>/js/app.js"></script>

<script>	new mlPushMenu( document.getElementById( 'mp-menu' ), document.getElementById( 'trigger' ) );</script>
</body>
</html>

<section id="get-in-touch">
  	<div class="container">
    <div class="row">
    	<div class="col-size-5">
        	<h3>Your future now</h3>
            <p>1/1 Atchison St,<br/> 
		St Leonards NSW 2065</p>

<p><span class="condensed">Phone</span><br/> 
<span itemprop="telephone">1800 552 168</span></p>

<p><span class="condensed">Email</span><br/>
<a href="mailto:">info@yourfuture.com.au</a></p>
        </div>
        <div class="col-size-7">
        	<h3>Get in touch</h3>

<?php echo do_shortcode( '[contact-form-7 id="257" title="Contact form 1"]' );?>
</div>
    </div>
    </div>
  </section><!-- GET IN TOUCH -->
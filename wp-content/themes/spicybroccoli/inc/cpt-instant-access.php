<?php
add_action( 'init', 'register_cpt_instant_access' );

function register_cpt_instant_access() {

    $labels = array( 
        'name' => _x( 'Lead Magnet', 'instant_access' ),
        'singular_name' => _x( 'Lead Magnet', 'instant_access' ),
        'add_new' => _x( 'Add New', 'instant_access' ),
        'add_new_item' => _x( 'Add New Lead Magnet', 'instant_access' ),
        'edit_item' => _x( 'Edit Lead Magnet', 'instant_access' ),
        'new_item' => _x( 'New Lead Magnet', 'instant_access' ),
        'view_item' => _x( 'View Lead Magnet', 'instant_access' ),
        'search_items' => _x( 'Search Lead Magnet', 'instant_access' ),
        'not_found' => _x( 'No lead magnet found', 'instant_access' ),
        'not_found_in_trash' => _x( 'No lead magnet found in Trash', 'instant_access' ),
        'parent_item_colon' => _x( 'Parent Lead Magnet:', 'instant_access' ),
        'menu_name' => _x( 'Lead Magnet', 'instant_access' ),
    );

    $args = array( 
        'labels' => $labels,
        'hierarchical' => false,
        
        'supports' => array( 'title', 'editor', 'thumbnail', 'custom-fields' , 'page-attributes' ),
        
        'public' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        
        'menu_icon' => 'dashicons-welcome-widgets-menus',
        'show_in_nav_menus' => true,
        'publicly_queryable' => true,
        'exclude_from_search' => false,
        'has_archive' => true,
        'query_var' => true,
        'can_export' => true,
        'rewrite' => true,
        'capability_type' => 'post'
    );

    register_post_type( 'instant_access', $args );
}
<?php
add_filter( 'init', function()
{
    global $wp;
    $wp->add_query_var( 'instant_access' );
	$wp->add_query_var( 'page_match' );
});

 add_action( 'init', 'sbm_rewrite', 9 );

function sbm_rewrite() {
	add_rewrite_rule('^lead-magnet/([a-zA-z\-]+)?$', 'index.php?post_type=course&instant_access=single&page_match=$matches[1]', 'top');
}

add_action( 'parse_query', function($query){
	global $wp_query;
	
	// Don't run this if in admin. Bail. Immediately. Get out. go. hurry.
	if (is_admin()) {
		return;
	}
	
	if ($wp_query->query_vars['instant_access'] == 'single') {
		$query->set('name', $wp_query->query_vars['page_match']);	
	} 
	return $query;

});
  
  add_filter('template_include', function($template) {
  global $wp_query;

	  if ($wp_query->query_vars['instant_access'] == 'single') {
		  
		$template = locate_template('single-lead-magnet.php');
	  } 
	  
	  return $template;
  
  });

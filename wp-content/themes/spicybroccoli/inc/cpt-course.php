<?php
add_action( 'init', 'register_cpt_course' );

function register_cpt_course() {

    $labels = array( 
        'name' => _x( 'Courses', 'course' ),
        'singular_name' => _x( 'Course', 'course' ),
        'add_new' => _x( 'Add New', 'course' ),
        'add_new_item' => _x( 'Add New Course', 'course' ),
        'edit_item' => _x( 'Edit Course', 'course' ),
        'new_item' => _x( 'New Course', 'course' ),
        'view_item' => _x( 'View Course', 'course' ),
        'search_items' => _x( 'Search Courses', 'course' ),
        'not_found' => _x( 'No courses found', 'course' ),
        'not_found_in_trash' => _x( 'No courses found in Trash', 'course' ),
        'parent_item_colon' => _x( 'Parent Course:', 'course' ),
        'menu_name' => _x( 'Courses', 'course' ),
    );

    $args = array( 
        'labels' => $labels,
        'hierarchical' => false,
        
        'supports' => array( 'title', 'editor', 'thumbnail', 'custom-fields' , 'page-attributes' ),
        
        'public' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        
        'menu_icon' => 'dashicons-welcome-learn-more',
        'show_in_nav_menus' => true,
        'publicly_queryable' => true,
        'exclude_from_search' => false,
        'has_archive' => true,
        'query_var' => true,
        'can_export' => true,
        'rewrite' => true,
        'capability_type' => 'post'
    );

    register_post_type( 'course', $args );
}
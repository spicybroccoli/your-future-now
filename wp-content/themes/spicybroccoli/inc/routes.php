<?php
add_filter( 'init', function()
{
    global $wp;
    $wp->add_query_var( 'show_header' );
});
  add_action( 'init', 'sbm_rewrite', 9 );
  
 function sbm_rewrite() {
	// to show pagination correctly
	add_rewrite_rule('^live-it-now\/page\/([0-9]+)?$', 'index.php?post_type=post&category_name=live-it-now&paged=$matches[1]', 'top');
	
	add_rewrite_rule('^sign-up\/([a-zA-z\-]+)?$', 'index.php?post_type=instant_access&name=$matches[1]&show_header=noshow', 'top');
		
 }